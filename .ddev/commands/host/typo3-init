#! /usr/bin/env bash

set -eu -o pipefail

# Vite add-on: provides "ddev vite-serve {start|stop}}" command
addonOutput=$(ddev get torenware/ddev-viteserve 2>&1) || (echo "$addonOutput" && exit 1)

ddev start
ddev composer install

# Dynamically set the "server-type" option for the setup command
# based on "DDEV_WEBSERVER_TYPE". DDEV does not support IIS!
[[ $DDEV_WEBSERVER_TYPE = apache* ]] && serverType="apache" || serverType="other"

ddev typo3 setup --server-type=$serverType --force -n

# Prepare frontend
ddev npm install
ddev npm run build:production

# Initialize data
ddev typo3 extension:setup
ddev composer dumpautoload

echo "TYPO3 is ready at ${DDEV_PRIMARY_URL}"
echo "      Username: $(ddev exec printenv TYPO3_SETUP_ADMIN_USERNAME)"
echo "      Password: $(ddev exec printenv TYPO3_SETUP_ADMIN_PASSWORD)"

# Open TYPO3 in the browser
ddev launch /typo3
